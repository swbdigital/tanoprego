import { combineReducers } from "redux";
import { navReducer } from "../../navigation";

import todos from "./todos";

export default combineReducers({
  nav: navReducer,
  todos: () => [
    {
      id: 1,
      text: "Fazer café"
    },
    {
      id: 2,
      text: "Fazer café 2"
    },
    {
      id: 3,
      text: "Fazer café 3"
    },
    {
      id: 4,
      text: "Fazer café 4"
    }
  ]
});
