import React, { Component, Platform } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView
} from "react-native";
import {
  Content,
  Form,
  Item,
  Input,
  Label,
  ListItem,
  Left,
  Right,
  Radio
} from "native-base";

import {
  StyledView,
  StyledText,
  StyledContainer,
  StyledViewContainerLogoApp,
  StyledTextContainerLogoApp,
  StyledButtonApp
} from "../styledHome";

import navigate from "react-navigation";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// import { requestTodoList } from "../redux/actions/todos";

class Home extends Component {
  constructor(props) {
    super(props);
  }

  sameFunction({ value }) {
    console.log("mudado", value);
  }

  render() {
    return (
      <ScrollView>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#bf8040",
            marginLeft: 60,
            marginRight: 60
          }}
        >
          <Text
            style={{
              color: "#e60000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Neste espaco vai a logomarca do App
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("HelpTypes")}
          style={{
            backgroundColor: "#e60000",
            width: 230,
            height: 230,
            marginTop: 19,
            borderWidth: 9,
            borderColor: "#fff",
            borderRadius: 29,
            alignSelf: "center",
            justifyContent: "center",
            alignItems: "center",
            alignContent: "center",
            shadowColor: "#ccc",
            shadowOffset: { width: 10, height: 9 },
            shadowOpacity: 1,
            shadowRadius: 20,
            elevation: 2
          }}
        >
          <Text style={{ color: "#fff", fontSize: 45, fontWeight: "bold" }}>
            SOS
          </Text>
        </TouchableOpacity>

        <Content style={{ marginLeft: 10, marginRight: 20, marginTop: 20 }}>
          <Form>
            <Item>
              <Label>Usuário</Label>
              <Input />
            </Item>
            <Item>
              <Label>Senha</Label>
              <Input secureTextEntry={true} />
            </Item>
          </Form>
          <Item
            underline={false}
            style={{
              borderColor: "transparent",
              marginTop: 20,
              paddingLeft: 16
            }}
          >
            {/* <Left> */}
            <Radio selected={false} />
            {/* </Left>
            <Right> */}
            <Label style={{ marginLeft: 20 }}>Manter-me conectado</Label>
            {/* </Right> */}
          </Item>
        </Content>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#bf8040",
            marginLeft: 90,
            marginRight: 5,
            marginTop: 30
          }}
        >
          <Text
            style={{
              color: "#e60000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Neste espaco vai a logomarca da empresa responsavel pelo app
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  todos: state.todos
});

// const mapDispatchToProps = dispatch =>
//   bindActionCreators(requestTodoList, dispatch);

export default connect(
  mapStateToProps
  // mapDispatchToProps
)(Home);
