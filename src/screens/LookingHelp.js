import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import {
  Content,
  Form,
  Picker,
  Item,
  Right,
  Left,
  Label,
  Input,
  Textarea,
  Button
} from "native-base";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { selectCategory } from "../redux/actions";

class LookingHelp extends Component {
  constructor(props) {
    super(props);
  }

  sameFunction({ value }) {
    console.log("mudado", value);
  }

  onValueChange(value) {
    debugger;
    console.log("value change", value);
    selectCategory(value);
  }

  verifyValueSelected() {
    debugger;
    console.log("ta entrando", this.state.categorySelected);
    return this.state.categorySelected === null
      ? ""
      : this.state.categorySelected;
  }

  render() {
    return (
      <ScrollView>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#bf8040",
            marginLeft: 60,
            marginRight: 60
          }}
        >
          <Text
            style={{
              color: "#e60000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Neste espaco vai a logomarca do App
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#000",
            marginLeft: 30,
            marginRight: 30,
            marginTop: 30,
            backgroundColor: "#ffff00"
          }}
        >
          <Text
            style={{
              color: "#000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            LOCALIZANDO SOCORRO...
          </Text>
        </View>

        <View
          style={{
            borderWidth: 1,
            borderColor: "#bf8040",
            marginLeft: 90,
            marginRight: 5,
            marginTop: 30
          }}
        >
          <Text
            style={{
              color: "#e60000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Neste espaco vai a logomarca da empresa responsavel pelo app
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  categorySelected: state.categorySelected
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ selectCategory }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LookingHelp);
