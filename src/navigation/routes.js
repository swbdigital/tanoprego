import { createStackNavigator } from "react-navigation";

import Home from "../screens/Home";
import ScreenHelpTypes from "../screens/ScreenHelpType";
import ConfirmPayment from "../screens/ConfirmPayment";
import LookingHelp from "../screens/LookingHelp";

const Routes = createStackNavigator(
  {
    Home: Home,
    HelpTypes: ScreenHelpTypes,
    ConfirmPayment: ConfirmPayment,
    LookingHelp: LookingHelp
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      title: "TÁ NO PREGO?",
      headerStyle: {
        backgroundColor: "transparent"
      },
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        color: "#000080",
        fontSize: 40
      },
      headerLeft: null
    }
  }
);

export default Routes;
