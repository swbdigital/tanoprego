import { createStore, applyMiddleware } from "redux";

/**
 * ESTADO DAS TELAS
 */
import reducers from "./reducers";

/**
 * GERENCIANDO ROTAS
 */
import { middleware } from "../navigation";

const store = createStore(reducers, applyMiddleware(middleware));

export default store;
