import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import {
  Content,
  Form,
  Picker,
  Item,
  Right,
  Left,
  Label,
  Input,
  Textarea,
  Button
} from "native-base";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { selectCategory } from "../redux/actions";

class HelpType extends Component {
  constructor(props) {
    super(props);
  }

  sameFunction({ value }) {
    console.log("mudado", value);
  }

  onValueChange(value) {
    debugger;
    console.log("value change", value);
    selectCategory(value);
  }

  verifyValueSelected() {
    debugger;
    console.log("ta entrando", this.state.categorySelected);
    return this.state.categorySelected === null
      ? ""
      : this.state.categorySelected;
  }

  render() {
    return (
      <ScrollView>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#bf8040",
            marginLeft: 60,
            marginRight: 60
          }}
        >
          <Text
            style={{
              color: "#e60000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Neste espaco vai a logomarca do App
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#000",
            marginLeft: 30,
            marginRight: 30,
            marginTop: 30,
            backgroundColor: "#ffff00"
          }}
        >
          <Text
            style={{
              color: "#000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Voce esta a 3 passos de confirmar seu pedido de socorro. PREENCHA OS
            CAMPOS ABAIXO
          </Text>
        </View>

        <View style={{ marginTop: 20, paddingRight: 25, paddingLeft: 10 }}>
          <Content>
            <Form>
              <Item>
                <Label>Tipo de socorro</Label>
                <Picker
                  mode="dropdown"
                  style={{ width: undefined }}
                  selectedValue={() => this.verifyValueSelected()}
                  onValueChange={this.onValueChange.bind(this)}
                >
                  <Picker.Item label="Tipo 1 socorro" value="type0" />
                  <Picker.Item label="Tipo 2 socorro" value="type1" />
                  <Picker.Item label="Tipo 3 socorro" value="type2" />
                  <Picker.Item label="Tipo 4 socorro" value="type3" />
                  <Picker.Item label="Tipo 5 socorro" value="type4" />
                </Picker>
              </Item>
              <Item>
                <Label>Tipo de servico</Label>
                <Picker
                  mode="dropdown"
                  style={{ width: undefined }}
                  selectedValue={""}
                  onValueChange={this.onValueChange.bind(this)}
                >
                  <Picker.Item label="Tipo 1 servico" value="type0" />
                  <Picker.Item label="Tipo 2 servico" value="type1" />
                  <Picker.Item label="Tipo 3 servico" value="type2" />
                  <Picker.Item label="Tipo 4 servico" value="type3" />
                  <Picker.Item label="Tipo 5 servico" value="type4" />
                </Picker>
              </Item>
              <Item>
                <Label>Localizacao</Label>
                <Input />
              </Item>
            </Form>
          </Content>
        </View>
        <View style={{ marginTop: 20, paddingRight: 10, paddingLeft: 13 }}>
          <Content padder>
            <Form>
              <Textarea
                rowSpan={5}
                bordered
                placeholder="Descreva o problema (com o máximo de detalhes):"
              />
            </Form>
            <Button
              primary
              style={{
                width: 100,
                flexDirection: "row",
                alignContent: "flex-end",
                alignSelf: "flex-end",
                justifyContent: "center",
                marginTop: 30
              }}
              onPress={() => this.props.navigation.navigate("ConfirmPayment")}
            >
              <Text style={{ color: "#fff" }}> Avancar </Text>
            </Button>
          </Content>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#bf8040",
            marginLeft: 90,
            marginRight: 5,
            marginTop: 30
          }}
        >
          <Text
            style={{
              color: "#e60000",
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            Neste espaco vai a logomarca da empresa responsavel pelo app
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  categorySelected: state.categorySelected
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ selectCategory }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HelpType);
