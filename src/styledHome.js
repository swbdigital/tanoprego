import styled from "styled-components/native";

const StyledView = styled.View`
  flex: 1;
`;

const StyledText = styled.Text`
  color: palevioletred;
`;

const StyledContainer = styled.View``;

const StyledViewContainerLogoApp = styled.View`
  border: 2px solid #bf8040;
  padding-bottom: 40px;
`;

const StyledTextContainerLogoApp = styled.Text`
  color: #ff0000;
`;

const StyledButtonApp = styled.TouchableOpacity`
  height: 20%;
  font-size: 10px;
  margin-left: 15%;
  margin-right: 15%;
  background: #e60000;
`;

export {
  StyledView,
  StyledText,
  StyledContainer,
  StyledViewContainerLogoApp,
  StyledTextContainerLogoApp,
  StyledButtonApp
};
